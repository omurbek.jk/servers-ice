#!/bin/bash

(
    cd /tmp/
    wget https://codeload.github.com/jonbrouse/docker-ice/zip/master

    unzip master
    cd docker-ice-master

    cp docker-compose-template.yml docker-compose.yml
    cp ice/assets/sample.properties ice/assets/ice.properties

    perl -i -pe 's{-Dice.s3AccessKeyId=<s3AccessKeyId>}{}g' docker-compose.yml
    perl -i -pe 's{-Dice.s3SecretKey=<s3SecretKeyId>}{}g' docker-compose.yml
    perl -i -pe "s{-Duser.timezone=<Your Timezone ie America/New_York>}{-Duser.timezone=$TIME_ZONE}g" docker-compose.yml

    perl -i -pe "s{ice.billing_s3bucketname=}{ice.billing_s3bucketname=$BILLING_BUCKET}g" ice/assets/ice.properties
    perl -i -pe "s{ice.companyName=}{ice.companyName=$COMPANY_NAME}g" ice/assets/ice.properties
    perl -i -pe "s{ice.work_s3bucketname=}{ice.work_s3bucketname=$BILLING_BUCKET}g" ice/assets/ice.properties
    echo "ice.account.production=${BILLING_ACCOUNT_NUMBER}" >> ice/assets/ice.properties
    
    docker-compose up -d
)
