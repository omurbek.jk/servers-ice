## This file was auto-generated by CloudCoreo CLI
## This file was automatically generated using the CloudCoreo CLI
##
## This config.rb file exists to create and maintain services not related to compute.
## for example, a VPC might be maintained using:
##
## coreo_aws_vpc_vpc "my-vpc" do
##   action :sustain
##   cidr "12.0.0.0/16"
##   internet_gateway true
## end

coreo_aws_vpc_vpc "${VPC_NAME}" do
  action :find
  cidr "${VPC_OCTETS}/16"
  tags ${VPC_TAGS}
end

coreo_aws_vpc_routetable "${PRIVATE_ROUTE_NAME}" do
  action :find
  vpc "${VPC_NAME}"
  tags [
        "Name=${PRIVATE_ROUTE_NAME}"
       ]
end

coreo_aws_vpc_subnet "${PRIVATE_SUBNET_NAME}" do
  action :find
  route_table "${PRIVATE_ROUTE_NAME}"
  vpc "${VPC_NAME}"
end

coreo_aws_ec2_securityGroups "${ICE_NAME}-elb-sg" do
  action :sustain
  description "Open http to the network"
  vpc "${VPC_NAME}"
  allows [ 
          { 
            :direction => :ingress,
            :protocol => :tcp,
            :ports => [80],
            :cidrs => ["${VPC_OCTETS}/16"]
          },{ 
            :direction => :egress,
            :protocol => :tcp,
            :ports => ["0..65535"],
            :cidrs => ["0.0.0.0/0"],
          }
    ]
end

coreo_aws_ec2_elb "${ICE_NAME}-elb" do
  action :sustain
  type "private"
  vpc "${VPC_NAME}"
  subnet "${PRIVATE_SUBNET_NAME}"
  security_groups ["${ICE_NAME}-elb-sg"]
  listeners [
             { :elb_protocol => 'http', :elb_port => 80, :to_protocol => 'http', :to_port => 80 }
            ]
  health_check_protocol "http"
  health_check_port 80
  health_check_path "/"
end

coreo_aws_route53_record "${ICE_NAME}.tools" do
  action :sustain
  type "CNAME"
  zone "${DNS_ZONE}"
  values ["COMPOSITE::coreo_aws_ec2_elb.${ICE_NAME}-elb.dns_name"]
end

coreo_aws_ec2_securityGroups "${ICE_NAME}" do
  action :sustain
  description "Security group for all ${ICE_NAME} servers"
  vpc "${VPC_NAME}"
  allows [ 
          { 
            :direction => :ingress,
            :protocol => :tcp,
            :ports => [22],
            :cidrs => ["${VPC_OCTETS}/16"]
          },{ 
            :direction => :ingress,
            :protocol => :tcp,
            :ports => [80],
            :groups => ["${ICE_NAME}-elb-sg"]
          },{ 
            :direction => :egress,
            :protocol => :tcp,
            :ports => ["0..65535"],
            :cidrs => ["0.0.0.0/0"]
          },{ 
            :direction => :egress,
            :protocol => :udp,
            :ports => ["0..65535"],
            :cidrs => ["0.0.0.0/0"]
          }
    ]
end

coreo_aws_iam_policy "${ICE_NAME}-billing" do
  action :sustain
  policy_name "Allow${ICE_NAME}Billing"
  policy_document <<-EOH
{
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
          "arn:aws:s3:::${BILLING_BUCKET}",
          "arn:aws:s3:::${BILLING_BUCKET}/*"
      ],
      "Action": [ 
          "s3:*"
      ]
    }
  ]
}
EOH
end

coreo_aws_iam_instance_profile "${ICE_NAME}" do
  action :sustain
  policies ["${ICE_NAME}-billing"]
end

coreo_aws_ec2_instance "${ICE_NAME}" do
  action :define
  image_id "${AWS_LINUX_AMI}"
  size "${ICE_SIZE}"
  security_groups ["${ICE_NAME}"]
  role "${ICE_NAME}"
  associate_public_ip false
  ssh_key "${ICE_SSH_KEY_NAME}"
end

coreo_aws_ec2_autoscaling "${ICE_NAME}" do
  action :sustain 
  minimum 1
  maximum 1
  server_definition "${ICE_NAME}"
  subnet "${PRIVATE_SUBNET_NAME}"
  health_check_grace_period 300
  elbs ["${ICE_NAME}-elb"]
  upgrade({
            :upgrade_on => "dirty",
            :cooldown => ${ICE_COOLDOWN},
            :replace => "${ICE_REPLACEMENT_TYPE}"
          })
  tags [
        "Name=${ICE_NAME}"
       ]
end

